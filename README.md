# Roulette-Clang-16.0.0

* Build completed on: 2022-09-29
* LLVM commit: https://github.com/llvm/llvm-project/commit/e62b3a93
* Clang Version: Roulette clang version 16.0.0 (https://github.com/llvm/llvm-project e62b3a9375d8694efe5bf3d1409bec9be0d288e9)
* Binutils version: 2.39
* Host Glibc: 2.31

